---
authors:
  - SPDX-FileCopyrightText: 2020 Bhushan Shah <bshah@kde.org>
SPDX-License-Identifier: CC-BY-4.0
title: "Plasma Mobile applications tarball releases"
created_at: 2021-02-23 11:45:00 UTC
author: Bhushan Shah
layout: post
---

Plasma Mobile team is happy to announce availability of tarballs for several of Plasma Mobile applications, they are available to download from download.kde.org

Following are their details,


- plasma-dialer: [https://download.kde.org/stable/plasma-dialer/0.2/](https://download.kde.org/stable/plasma-dialer/0.2/)
```
a228f6d41b5524bfbb0a63fdeb28dfb71c31c4a0468afda86769931a52373102  plasma-dialer-0.2.tar.xz.sig
92c2425ebd0aaaa074b69c6b243fbdbe090468c9207f8f1cb0f77e1b9c2aa31b  plasma-dialer-0.2.tar.xz
```

- spacebar: [http://download.kde.org/stable/spacebar/0.1/](http://download.kde.org/stable/spacebar/0.1/)
```
1eea821ad160a45a39cfae13fae18cc0ffde353a4c1365db0b764354fbd120f0  spacebar-0.1.tar.xz.sig
678746dca09866fae1d1ffe2f5e267c16f10f739abaeca6981762938e36fbb5b  spacebar-0.1.tar.xz
```

- angelfish: [https://download.kde.org/stable/angelfish/1.8.0/](https://download.kde.org/stable/angelfish/1.8.0/)
```
88d57876dc28fff9fad79c99d2d239282f9cd24bcd372152084c2ca7a91f1845  angelfish-1.8.0.tar.xz.sig
def9a40de962c634304eadb91d505aa21faa70d327270e9de85b34c471ca8cd8  angelfish-1.8.0.tar.xz
```

- plasma-settings: [https://download.kde.org/stable/plasma-settings/0.1](https://download.kde.org/stable/plasma-settings/0.1)
```
ec7fee1b4dcf07f98be5a631de397638e035608d52ed3fbc074f15a7c6697e5b  plasma-settings-0.1.tar.xz.sig
909123625ba916bcf3ef2e35077817387b84f4f28e0a81368ac8465f86d538c9  plasma-settings-0.1.tar.xz
```

- alligator: [https://download.kde.org/stable/alligator/0.1](https://download.kde.org/stable/alligator/0.1)
```
f1ccb64c88ab9c8a5d680e3d3a79795e2174b4acf44cb92cfa34810935ce4d1a  alligator-0.1.tar.xz.sig
bca28252907cee344e551bd0801c5fff58d52f1c033dabf1c82bbcfdb14fe051  alligator-0.1.tar.xz
```

- plasma-phonebook: [http://download.kde.org/stable/plasma-phonebook/0.1/](http://download.kde.org/stable/plasma-phonebook/0.1/)
```
11185c7358dfc82e8b86473b967b7333c16db52406c8802e4d425798afdb9e81  plasma-phonebook-0.1.tar.xz.sig
c8cf9bbd3518353b30402b233e1ce1506a5878aa2d319dc2b8d4aa777ffbd68e  plasma-phonebook-0.1.tar.xz
```

- kclock: [https://download.kde.org/unstable/kclock/0.4/](https://download.kde.org/unstable/kclock/0.4/)
```
f548be1fafbd6041d09f4c4f2a8e48b9a0da607a1acdbc762291924529cfd6d6  kclock-0.4.0.tar.xz.sig
434b66e3c88a799e3f39e161620a0f944b3d0782f3ac54f10bd3c9e3912903e8  kclock-0.4.0.tar.xz
```

- kalk: [https://download.kde.org/unstable/kalk/0.2/](https://download.kde.org/unstable/kalk/0.2/)
```
124e136d4b5734bd93f9eed719a88ea01d27cda42a92373db0f4e30fb8398dd3  kalk-v0.2.tar.xz
13c4a2dff831f7eb5ee7521f531ce2a4888a90bf7fc8e65d3e4062d89c1c2d90  kalk-v0.2.tar.xz.asc
```

- kweather: [https://download.kde.org/unstable/kweather/0.4/](https://download.kde.org/unstable/kweather/0.4/)
```
e2847139663eecba36b27c6e050b80235ec9fa1c82e8530b36c8f0d67e14c32c  kweather-0.4.tar.xz
edf976d24408909f5205b14366851fab584dd21477ba2e9c08bf4076a3b4a901  kweather-0.4.tar.xz.asc
```

All of the tarballs are signed with my PGP key,

0AAC775BB6437A8D9AF7A3ACFE0784117FBCE11D bshah at mykolab dot com


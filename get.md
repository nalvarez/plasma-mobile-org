---
title: Get Plasma Mobile
permalink: /get/
layout: page
---

Distributions offering Plasma Mobile
====================================

**Mobile**
----------


Manjaro ARM
-----------

![](/img/manjaro.svg){:width="100"}

Manjaro ARM is the Manjaro distribution, but for ARM devices. It's based on Arch Linux ARM, combined with Manjaro tools, themes and infrastructure to make install images for your ARM device.

[Website](https://manjaro.org){:.learn-more} [Forum](https://forum.manjaro.org/c/arm/){:.learn-more}

Download:
[PinePhone daily (latest)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases){:.learn-more} [Releases](https://osdn.net/projects/manjaro-arm/storage/pinephone/plasma-mobile/){:.learn-more}

Neon based reference rootfs
-----------------------------------------

![](/img/neon.svg){:width="100"}

Image based on KDE Neon. Targeted at newer devices (with 1GB+ RAM).
KDE Neon itself is based upon Ubuntu 20.04 (focal). This image is based on the dev-unstable branch of KDE Neon, and always ships the latest versions of KDE frameworks, KWin and Plasma Mobile compiled from git master.

Download:
[PinePhone](https://images.plasma-mobile.org/pinephone/){:.learn-more}


openSUSE
--------

![](/img/openSUSE.svg){:width="100"}

openSUSE, formerly SUSE Linux and SuSE Linux Professional, is a Linux distribution sponsored by SUSE Linux GmbH and other companies. Currently openSUSE provides Tumbleweed based Plasma Mobile builds.

Download
[PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz){:.learn-more}

postmarketOS
------------

![](/img/pmOS.svg){:width="100"}

PostmarketOS (pmOS), is a touch-optimized, pre-configured Alpine Linux that can be installed on smartphones and other mobile devices. The project is at very early stages of development and is not usable for most users yet.

[Learn more](https://postmarketos.org){:.learn-more}

Download:
[PinePhone](https://images.postmarketos.org/pinephone/){:.learn-more}


Installation
-------------
Download the image, uncompress it and flash it to a SD-card using `dd` or a graphical tool. The Pine Phone will automatically boot from a SD-Card. To install to the embedded flash, please follow the instructions in the [Pine wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).



**Desktop Devices**
-----------

Neon based amd64 ISO image
---------------------------

![](/img/neon.svg){:width="100"}

This ISO image is using the same packages as the Neon based reference rootfs, just compiled for amd64. It can be tested on non-android intel tablets, PCs and virtual machines.

[Download](https://files.kde.org/neon/images/mobile/){:.learn-more}

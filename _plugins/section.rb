require 'dimensions'

module Jekyll
  class SectionTagBlock < Liquid::Block

    def initialize(tag_name, input, tokens)
      super
      @input = input
    end

    def markdown_converter
      @context.registers[:site].find_converter_instance(::Jekyll::Converters::Markdown)
    end

    def render(context)
      text = super

      direction = "left"
      app_name = ""
      app_img = ""
      app_img_shadow = "false"
      app_img_alt = ""

      # Attempt to parse the JSON if any is passed in
      begin
        if( !@input.nil? && !@input.empty? )
          jdata = JSON.parse(@input)
          if( jdata.key?("direction") )
            direction = jdata["direction"].strip
          end
          if( jdata.key?("app_img_shadow") )
            app_img_shadow = jdata["app_img_shadow"].strip
          end
          if( jdata.key?("app_name") )
            app_name = jdata["app_name"].strip
          end
          app_img = jdata["app_img"].strip
          app_img_alt = jdata["app_img_alt"].strip
        end
      rescue
      end
      file_name = app_img
      file_name[0] = ''
      size = Dimensions.dimensions(file_name)
      output = "<section class='app-announcement #{direction}'>"
      output += "<img src='/#{app_img}' class='shadow-#{app_img_shadow}' alt='#{app_img_alt}' width='#{size[0]}' height='#{size[1]}' />"
      output += "<div>"
      if app_name == ""
        output += "#{Kramdown::Document.new(text).to_html}"
      else
        output += "<h3 class='mt-0'>#{app_name}</h3>#{Kramdown::Document.new(text).to_html}"
      end
      output += "</div>"
      output += "</section>"
      return output;
    end

  end
end

Liquid::Template.register_tag('section', Jekyll::SectionTagBlock)
